/*import logo from './logo.svg';*/
import './App.css';
import React from 'react';
/*import TodoList from './TodoList/todoList';
import TodoItem from './TodoItem/todoItem';
import AddTodo from './AddTodo/addTodo';*/
import Header from './components/header';
import TodoInput from './components/todoInput';
import TodoItem from './components/todoItem';

class App extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      todos: [
        {id:0, Text: "Make dinner tonigth!"},
        {id:1, Text: "Fold the laundry."},
        {id:2, Text: "Learn to make a React app!"}
      ],
      nextId: 3
    }

    this.addTodo = this.addTodo.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
  }


  addTodo(todoText){
    let todos = this.state.todos.slice();
    todos.push({id: this.state.nextId, Text: todoText});
    this.setState({
      todos: todos,
      nextId: ++this.state.nextId
    });
  }

  removeTodo(id){
    this.setState({
      todos: this.state.todos.filter((todo,index) =>todo.id !==id)
    })
  }

  render(){
    return(
      <div className='App'>
        <div className='todo-wrapper'>
          <Header />
          <TodoInput  todoText="" addTodo={this.addTodo}/>
          <ul>
            {
              this.state.todos.map((todo) => {
                return <TodoItem todo={todo} key={todo.id} id={todo.id} removeTodo={this.removeTodo}/>
              })
            }
          </ul>
        </div>
      </div>
    )
  }

  /*constructor(){
    super();
    this.state = {
      todos: []
    };
  }

  render(){
    return(
      <div>
      <TodoList></TodoList>
      <TodoItem></TodoItem>
      Hello from the app component
    </div>
    );
  }


  componentDidMount = () => {
    const todos = localStorage.getItem('todos');
    if(todos){
      const savedTodos = JSON.parse(todos);
      this.setState({ todos: SavedTodos});
    } else{
      console.log('No todos');
    }
  }*/


}

/*function App() {
  return (
    <div className="App">
      <header className="App-header">
      
        <div>
          batuhan
        </div>
      </header>
    </div>
  );
}*/

export default App;
